import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sharedMessage: 'Sharing is caring',
    currentHeartColor: 'red',
    heartColors: {
      red: '❤️',
      blue: '💙',
      green: '💚',
      orange: '🧡',
      yellow: '💛'
    }
  },
  mutations: {
    changeHeartColor(state, color) {
      state.currentHeartColor = color
    }
  },
  actions: {
    setHeartColor({ commit, state }, color) {
      if ((typeof(color) === "string") &&(color in state.heartColors)) {
        commit('changeHeartColor', color)
      }
    }
  },
  getters: {
    getHeart: state => {
      return {message: state.sharedMessage, heart: state.heartColors[state.currentHeartColor]}
    },
    getAllHearts: state => {
      return state.heartColors
    }
  }
})
